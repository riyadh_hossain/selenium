package assignment4;

public class Assignment0302 {
	
	public double interestrate;
	public int time;
	public double principle;
	
	Assignment0302(double interest, int time, double principle)
	{
		this.interestrate = interest;
		this.time = time;
		this.principle = principle;
	}
	
	public double simpleinterest()
	{
		double result;
		double finalrate = interestrate();
		
		result = principle*(1+finalrate*this.time);
		
		return result;
	}
	
	
	public double interestrate() {
		
		double rate = this.interestrate/100;
		return rate;
		
	}
	

}
