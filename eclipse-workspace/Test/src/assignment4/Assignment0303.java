
/*
 * Write a Java program to concatenate a given string to the end of another string
 */

package assignment4;
import java.util.Scanner;

public class Assignment0303 {

	
	String first;
	String second;
	String finalresult;
	
	public String concatenate(String x, String y) {
		
		finalresult = x + y;
		
		return finalresult;
	}
	
	
	
	public static void main(String[] args) {
		
		String finalz;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Please enter the first string: ");
		
		String first = scan.next();
		
		System.out.print("Please enter the second string: ");
		
		String second = scan.next();
		scan.close();
		
		
		
		Assignment0303 x = new Assignment0303();
		
		finalz = x.concatenate(first, second);
		
		
		System.out.println("Result is: " + finalz);

		
		

	}

}
