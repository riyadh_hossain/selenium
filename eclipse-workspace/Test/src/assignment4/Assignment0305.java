/*
	class A
	{
	    int i = 10;
	}
	 
	class B extends A
	{
	    int i = 20;
	}
	 
	public class MainClass
	{
	    public static void main(String[] args)
	    {
	        A a = new B();
	 
	        System.out.println(a.i);
	    }
	}
	
	

*/
/*

ANS: This will give a Compilation Error for line 16. 
We are creating an object of class A but initiating with class B.


*/