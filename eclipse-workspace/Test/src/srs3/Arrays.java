package srs3;

public class Arrays {

	public static void main(String[] args) {
		int []a = {10,20,30,40};
		//System.out.println(a[2]);
		
		int []b = new int[10];
		//System.out.println(b[3]);
		
	/*for (int i = 0; i<=a.length; i++) {
		System.out.println("this is regular for loop for array" + a[i]);
		
		
	}*/
		
		//enhanced forloop
	for (int k:a) {
	System.out.println("this is enhanced for loop "+k);	
	}
	

	
	
	
	String str = "busyQA";
	String strg ="busyqA";
	
	System.out.println(str.charAt(3));
	System.out.println(str.substring(4));
	System.out.println(str.substring(4,5));
	
	System.out.println(str.concat("hello"));
	System.out.println(str.equalsIgnoreCase(strg));
	System.out.println(str.equals(strg));
	
	
	
	}


}