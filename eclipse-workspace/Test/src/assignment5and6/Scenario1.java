package assignment5and6;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class Scenario1 {

	public static WebDriver driver;

	public static void openbrowser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\Riyadh\\\\Desktop\\\\QA Automation\\\\Software\\\\chromedriver_win32\\\\chromedriver.exe");
		driver = new ChromeDriver();


		driver.get("http://newtours.demoaut.com/");

	}

	public static void main(String[] args) throws InterruptedException {


		printalllink();
		printusernameandpass();
		printtextvalueofsignin();





		Thread.sleep(3000);
		driver.quit();



	}



	public static void printalllink() throws InterruptedException
	{
		openbrowser();
		// printing total number of link

		int count = driver.findElements(By.tagName("a")).size();
		if(count>0)
		{
			System.out.println("Testing passed for total number count of hyperlink : ");
			System.out.println(count);
		}

		int countele =0;
		// printing all the link present
		List <WebElement> link = driver.findElements(By.tagName("a"));

		for( WebElement i: link)
		{
			System.out.println(i.getText());
			countele++;

		}

		if(countele==count)
		{
			System.out.println("Testing passed cause total number of hyperlink printed matches total count for hyperlink: ");
		}

		else
			System.out.println("Testing FAILED cause total number of hyperlink printed does not match total count for hyperlink: ");


		Thread.sleep(3000);
		driver.quit();


	}

	public static void printusernameandpass() throws InterruptedException
	{
		openbrowser();
		Thread.sleep(3000);
		WebElement name1 =driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/form/table/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/input"));
		String s1= name1.getAttribute("name");
		if(s1!=null)
		{
			System.out.print("Testing passedd. Printing the attribute value for username field: ");
			System.out.println(s1);
		}
		else
			System.out.println("Testing failed to get attribute for username field");

		WebElement pass = driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/form/table/tbody/tr[4]/td/table/tbody/tr[3]/td[2]/input"));
		String s = pass.getAttribute("name");

		if(s!=null)
		{
			System.out.println("Testing passed. Attribute for password field =  " +pass.getAttribute("name"));
		}

		else
			System.out.println("Testing failed to get attribute for password field ");

		Thread.sleep(3000);
		driver.quit();

	}



	public static void printtextvalueofsignin() throws InterruptedException

	{
		openbrowser();
		Thread.sleep(3000);
		// printing text value of Sign-In button , Your script should find the Sing-in button and print the text �Sign-in�.

		WebElement login = driver.findElement(By.cssSelector("input[name=login]"));

		String s3 = login.getAttribute("alt");
		if(s3!=null)
		{
			System.out.println("Testing passed. Text value for signin button =  " + s3);
		}
		else

			System.out.println("Testing failed to get the value of sign in button");


		Thread.sleep(3000);
		driver.quit();


	}




}
