package assignment5and6;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Scenario3 {

	public static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		
		
		applyforjob();

	}
	
	
	public static void openbrowser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\Riyadh\\\\Desktop\\\\QA Automation\\\\Software\\\\chromedriver_win32\\\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://tvo.org/");
		
	}
	
	
	public static void applyforjob() throws InterruptedException
	{
		openbrowser();
		
		Thread.sleep(2000);
		JavascriptExecutor js =  (JavascriptExecutor) driver;
		
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		
		driver.findElement(By.linkText("Jobs at TVO")).click();
		driver.findElement(By.linkText("Current Job Postings")).click();
		
		Set <String> s = driver.getWindowHandles();
		
		for(String i: s)
		{
			driver.switchTo().window(i);
			
		}
		
		driver.switchTo().frame("icims_content_iframe");
		driver.findElement(By.id("jsb_f_keywords_i")).sendKeys("automation");
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("input[id = jsb_form_submit_i]")).click();
		
		String s4= driver.findElement(By.xpath("/html/body/div[2]/div[3]")).getText();
		
		if(s4.equals("Sorry, no jobs were found that match your search criteria. Please try other selections."))
		{
			System.out.println("Testing passed");
		}
		
		Thread.sleep(5000);
		driver.quit();
	}
	
	
	

}
