package assignment5and6;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Scenario2 {

	public static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {

		additionofpayee();

	}


	public static void openbrowser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\Riyadh\\\\Desktop\\\\QA Automation\\\\Software\\\\chromedriver_win32\\\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://zero.webappsecurity.com/");

	}


	public static void additionofpayee() throws InterruptedException
	{
		openbrowser();

		driver.findElement(By.xpath("//*[@id=\'signin_button\']")).click();

		driver.findElement(By.xpath("//*[@id=\'user_login\']")).sendKeys("username");
		driver.findElement(By.xpath("//*[@id=\'user_password\']")).sendKeys("password");
		driver.findElement(By.xpath("//*[@id=\'login_form\']/div[2]/input")).click();

		WebElement s =	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/h2[1]"));

		if(s.getText().equals("Cash Accounts"))
		{
			System.out.println("Testing passed succesfully login: ");
		}

		driver.findElement(By.linkText("Pay Bills")).click();
		driver.findElement(By.linkText("Add New Payee")).click();
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("input[name=name]")).sendKeys("HydroOne Utility");
		driver.findElement(By.cssSelector("textarea[name=address]")).sendKeys("200 RoberSpec Pkwy, Mississauga, ON L6R1K9");
		driver.findElement(By.cssSelector("input[name=account]")).sendKeys("123234434");
		driver.findElement(By.cssSelector("input[name=details]")).sendKeys("Natural Gas Utility");
		driver.findElement(By.cssSelector("input[id=add_new_payee]")).click();
		
		String s1 = driver.findElement(By.id("alert_content")).getText();
		
		if(s1.equals("The new payee HydroOne Utility was successfully created."))
		{
			System.out.println("Testing passed:  Successfully added a new payee");
		}
		
		

		Thread.sleep(3000);
	    driver.quit();

	}





}
