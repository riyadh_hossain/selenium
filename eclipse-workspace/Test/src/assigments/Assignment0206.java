
/*
 * Write a program to calculate simple interest
 */
package assigments;

import java.util.Scanner;
public class Assignment0206 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the interest Rate: ");
		int rate = scan.nextInt();
		
		System.out.print("Enter the number of years : ");
		int year = scan.nextInt();
		
		System.out.print("Enter the Principal value : ");
		int principal = scan.nextInt();

		scan.close();
		
		int interest = rate/100;
        
		int result = principal*(1+interest*year);
		
		System.out.print("Simple Interest is = " + result);
		
		
	}

}
