
/*
 * Write a Java program for simple calculator application that does addition, multiplication, subtraction and division of two numbers
 */
package assigments;

import java.util.Scanner; 

public class Assignment0203 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter FIrst Number: ");
		int num1 = scan.nextInt();
		System.out.print("Enter Second Number: ");
		int num2 = scan.nextInt();
		
		System.out.print("Choose an option from below:  1 = Addition 2= Deduction 3= Multiplication 4=Division");
		int option = scan.nextInt();
		
		if (option == 1)
		{
			System.out.println(num1+num2);
		}
		else if (option ==2)
		{
			System.out.println(num1-num2);
		}
		
		else if (option ==3)
		{
			System.out.println(num1*num2);
		}
		
		else if (option ==4)
		{
			System.out.println(num1/num2);
		}
		
		else
			System.out.println("You have entered an invalid option");
			
		

	}

}
