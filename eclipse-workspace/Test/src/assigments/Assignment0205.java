
/*
 * Write a program to calculate area of circle
 */
package assigments;

import java.util.Scanner;

public class Assignment0205 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the Radius of the circle: ");
        int radius = scan.nextInt();
        scan.close();
        
        System.out.print("Area of the Circle = " + Math.PI*radius*radius);
        
        
        
        
        
	}

}
