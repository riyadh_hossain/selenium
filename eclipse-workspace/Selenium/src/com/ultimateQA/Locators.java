package com.ultimateQA;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Locators {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// to go to the link
		driver.get("https://www.ultimateqa.com/simple-html-elements-for-automation/");
		//locator id
		driver.findElement(By.id("button1")).click();
		//Go back the webpage
		driver.navigate().back();// thats to go back to previous page
		
		
		// locator by class name
		driver.findElement(By.className("buttonClass")).click();
		//Thread.sleep(5000);
		driver.navigate().back();
		
		
		//Locator by name
		driver.findElement(By.name("button1")).click();
		//Thread.sleep(5000);
		driver.navigate().back();
		
		
		//locator by linktext
		driver.findElement(By.linkText("Click me using this link text!")).click();
		Thread.sleep(5000);
		driver.navigate().back();
		
		//locator PartialText
		driver.findElement(By.partialLinkText("Click me using this link")).click();
		Thread.sleep(5000);
		driver.navigate().back();
		
		
		//locator CSS Selector
		driver.findElement(By.cssSelector("#button1")).click(); //this is CSS selector for locator id. Amylocator can be done in css selector
		driver.findElement(By.cssSelector(".buttonClass")).click(); //css selector locator class name
		
		
		//Css selector by Attribute
		
		driver.findElement(By.cssSelector("[class= 'buttonClass'][type='submit']")).click();
		
		
		
		
		
		
		
		driver.close();
		
		

	}
}
