package com.google;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


public class Intro {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
//		
//		System.setProperty("webdriver.ie.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe");
//		WebDriver driver = new InternetExplorerDriver();
		
//		System.setProperty("webdriver.edge.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\IEDriverServer.exe");
//		WebDriver driver = new EdgeDriver();
		
		 
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		
		driver.manage().window().maximize();
		
		System.out.println(driver.getCurrentUrl());
		
		driver.close();
	}
}
