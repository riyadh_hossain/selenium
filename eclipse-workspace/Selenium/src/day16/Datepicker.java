package day16;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Datepicker {

	public static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		 driver = new ChromeDriver();
		driver.get("http://demo.automationtesting.in/Datepicker.html");
		driver.manage().window().maximize();
		
		
		currentdate();
		futuremonth("January", "20", "2009");
		pastdate("January", "20", "2009");
		
		
		
		
	}
	
	
	
	public static void currentdate() throws InterruptedException
	{
		
		//current date
				driver.findElement(By.xpath("//*[@id=\'datepicker2\']")).click();
				Thread.sleep(3000);
				driver.findElement(By.linkText("13")).click();
	}
	
	
	
	
	
	public static void futuremonth(String month, String day, String year) throws InterruptedException
	{
		
		String m = month;;
		String d = day;
		String y = year;
		
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\'datepicker2\']")).click(); // clicking on calender
		Thread.sleep(3000);
		//driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div/select[1]")).click(); //either use this one or the one below both works 
		driver.findElement(By.cssSelector("select[class='datepick-month-year']")).click(); // clicking on month drop down
		
		Select mon = new Select (driver.findElement(By.cssSelector("select[class='datepick-month-year']")));
		

		List <WebElement> dropdown = mon.getOptions();
		Thread.sleep(5000);
		for(WebElement i:dropdown)
		{
		
			
			
			String optionmonth = i.getText();
			
			if(m.equals(optionmonth))
			{
				mon.selectByVisibleText(optionmonth);
				driver.findElement(By.linkText(d)).click();
				break;
				
			}
			
		}
		
		
		
	}
	
	
	
	
	public static void pastdate(String mon, String day, String year) throws InterruptedException
	{
		String m= mon;
		String d = day;
		String y = year;
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\'datepicker2\']")).click(); // clicking on calender
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div/select[2]")).click();
		
		
		//choosing year
		Select year1 = new Select (driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div/select[2]")));
		List <WebElement> allyear =  year1.getOptions();
		
		for(WebElement c: allyear)
		{
			
			String newy = c.getText();
			
			if(y.equals(newy))
			{
				//System.out.println(i.getText());
				year1.selectByVisibleText(y);
				break;
			}
			
		}
		
		//selecting mont
        driver.findElement(By.cssSelector("select[class='datepick-month-year']")).click(); // clicking on month drop down
		
		Select month1 = new Select (driver.findElement(By.cssSelector("select[class='datepick-month-year']")));
		

		List <WebElement> dropdown1 = month1.getOptions();
		Thread.sleep(5000);
		for(WebElement i:dropdown1)
		{
			
			String optionmonth = i.getText();
			
			if(m.equals(optionmonth))
			{
				month1.selectByVisibleText(optionmonth);
				driver.findElement(By.linkText(d)).click();
				break;
				
			}
			
		}
		
		Thread.sleep(5000);
		driver.quit();
		
		
	
	}
	
	
	
	

}
