package day14;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Volunteersignup {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.ultimateqa.com/simple-html-elements-for-automation/");
		//driver.manage().window().maximize();
		
		//inputbox
		driver.findElement(By.name("et_pb_contact_name_1")).sendKeys("john");
		driver.findElement(By.name("et_pb_contact_email_1")).sendKeys("asdasd@sdsdds.com");
		
		
		
		//dropdown
		
		Select dropdown = new Select(driver.findElement(By.tagName("Select")));
		System.out.println(dropdown.getOptions().size());//find out how many options available for the drop down down menu
		
		// How to capture extract all the option value and print them in a console
		List <WebElement> options = dropdown.getOptions();		
		for(WebElement e : options )
		{
			System.out.println(e.getText()); //Extract text value of each drop down options
			
		}
	
	/*	How to select an option from drop down
		There are 3 ways we can do it
		1. select�ByIndex
		2. select�ByValue
		3. select�ByV�isi�bleText
		*/
		
		
		dropdown.selectByIndex(2);// this value for index is like an array from the dropdown list
		dropdown.selectByValue("audi"); // Value is an attribute thats taken from html. this is not the same as visible option value
		dropdown.selectByVisibleText("Volvo");
		
		
		
		/*
		Radio button
		 * 1. Click/select  the radio button
		 * 2. isselected()  to see if the option is choosen
		 */
		
		//Radio button
		
		System.out.println(driver.findElement(By.cssSelector("input[name='gender'][value='male']")).isSelected());
		//Thread.sleep(9000);
		
		//driver.findElement(By.cssSelector("input[name='gender'][value='male']")).click(); //select a radio button using css selector
	    System.out.println(driver.findElement(By.cssSelector("input[name='gender'][value='male']")).isSelected()); // checking if the radio button is selected or not by returning true or false
	    
	    
	    //check boxes
		
	    driver.findElement(By.cssSelector("input[type='checkbox'][value='Car']")).click();
	    driver.findElement(By.cssSelector("input[type='checkbox'][value='Bike']")).click();
		
				
		//count number of links in a web page
	    
	    
	    
	    //findelement can locate single element based on locator we provide
	    //findelements can identify more than one element based on locator we provide. But this locator should match more elements
	    
	    System.out.println(driver.findElements(By.tagName("a")).size());// finding all the links by using tag a which is common for all a href
		
	    List <WebElement> options1 = driver.findElements(By.tagName("a"));
	    
	    for(WebElement x: options1 )
	    {
	    	System.out.println(x.getText());
	    }
	    
	    
	   
	    
	    
	    
	    
	    
	}
}

