package day14;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Alertandpopup {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.automationtesting.in/Alerts.html");



		//Alert/pop up boxes

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/ul/li[2]/a")).click(); //alert with ok and cancel button

		driver.findElement(By.xpath("//*[@id=\'CancelTab\']/button")).click();
		driver.switchTo().alert().accept(); // this is a fixed command to click ok/yes in a pop up box
		String text = driver.findElement(By.xpath("//*[@id=\"demo\"]")).getText();
		if(text.equals("You pressed Ok"))
		{
			System.out.println("Test pass with proper text");

		}

		else 
			System.out.println("Test failed no text");

		// Alert pop up clicking cancel

		driver.findElement(By.xpath("//*[@id=\'CancelTab\']/button")).click();
		driver.switchTo().alert().dismiss(); // this is afixed command to click cancel in a pop up.

		String text1 = driver.findElement(By.xpath("//*[@id=\"demo\"]")).getText();
		if(text1.equals("You Pressed Cancel"))
		{
			System.out.println("Test pass with proper text");

		}

		else 
			System.out.println("Test failed no text");


		//Thread.sleep(9000);

		


	}


}
