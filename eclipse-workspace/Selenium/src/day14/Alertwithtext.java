package day14;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alertwithtext {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.automationtesting.in/Alerts.html");
		
		// Alert with input box



				driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/ul/li[3]/a")).click(); //alert with ok and cancel button

				driver.findElement(By.xpath("//*[@id=\'Textbox\']/button")).click();

				driver.switchTo().alert().sendKeys("Riyadh");
				driver.switchTo().alert().accept();

				
				String text1 = driver.findElement(By.xpath("//*[@id=\'demo1\']")).getText();
				if(text1.equals("Hello Riyadh How are you today"))
				{
					System.out.println("Test pass with proper text");

				}

				else 
					System.out.println("Test failed no text");
	}

}
