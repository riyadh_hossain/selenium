/*Actions class
-------------
1) MouseHover  ---> moveToElement(target element).click().build().perform();
2) Right click ---> contextClick(target element).build().perform();
3) Double click --> doubleClick(target element).build().perform();
4) drag and drop --> dragAndDrop(sourceelement, targetelement).build().perform();
5) Resize --> moveToElement(sourceelement).dragAndDropBy(sourceelement, 150, 100).build().perform();
6) Slider--> moveToElement(action7).dragAndDropBy(action7, 300, 0).build().perform();
7)Scrolling page-- Use javascriptexecutor create an object and use that object -->JavascriptExecutor cs =  (JavascriptExecutor) driver;-->cs.executeScript("arguments[0].scrollIntoView();", targetelement);
	a) scroll down till you reach a certain pixel number --> js.executeScript("window.scrollBy(0,500)", "")
	b)  scroll down till you reach a certain element   --> js.executeScript("arguments[0].scrollIntoView();",  element); 
	d) scroll down till you reach bottom of the page

 */

package day17;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Actionsclassmouse {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		/*
		driver.get("http://demo.automationtesting.in/WebTable.html");

        WebElement admin = driver.findElement(By.xpath("//*[@id=\"header\"]/nav/div/div[2]/ul/li[4]/a"));
		WebElement admin2 = driver.findElement(By.xpath("//*[@id=\"header\"]/nav/div/div[2]/ul/li[4]/ul/li[1]/a"));
		WebElement admin3 = driver.findElement(By.xpath("//*[@id=\"header\"]/nav/div/div[2]/ul/li[4]/ul/li[3]/a"));

		Thread.sleep(3000);
		Actions act = new Actions(driver);

		//Mouse over action
		act.moveToElement(admin).moveToElement(admin2).moveToElement(admin3).click().build().perform();


		//Right click action

		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");

		Actions act2 = new Actions(driver);
		WebElement action =	driver.findElement(By.xpath("/html/body/div/section/div/div/div/p/span"));

		act2.contextClick(action).build().perform();

		driver.findElement(By.xpath("/html/body/ul/li[3]/span")).click(); // clicking on the copy button

		String s = driver.switchTo().alert().getText(); // after clicking switching to alert menu box and getting the text of the alert box
		System.out.println(s);

		driver.switchTo().alert().accept(); //clicking ok in the alert box




		//Scroll down the paghe till element appears
		// To do this we have use javascript executer class

		driver.get("http://api.jquery.com/dbclick/");


		WebElement action3 = driver.findElement(By.xpath("//*[@id=\'post-0\']/header/h1"));
		Actions act3 = new Actions(driver);



		Thread.sleep(2000);
		JavascriptExecutor cs =  (JavascriptExecutor) driver;
		cs.executeScript("arguments[0].scrollIntoView();", action3);


		// double click

		Actions act4 = new Actions(driver);
		act4.doubleClick(action3).build().perform();



		//drag and drop

		driver.get("http://www.dhtmlgoodies.com/scripts/drag-drop-custom/demo-drag-drop-3.html");

		WebElement sourceele = driver.findElement(By.xpath("//*[@id=\'box6\']"));
		WebElement targetele = driver.findElement(By.xpath("//*[@id=\'box106\']"));
		Actions act5 = new Actions(driver);

		act5.dragAndDrop(sourceele, targetele).build().perform();





		// Resizable 

		driver.get("https://jqueryui.com/resizable/");


		Actions act6 = new Actions(driver);
		driver.switchTo().frame(0); //Switching to the iframe

		WebElement action6 = driver.findElement(By.xpath("//*[@id=\'resizable\']/div[3]"));

		Thread.sleep(3000);
		act6.moveToElement(action6).dragAndDropBy(action6, 150, 100).build().perform();




		//Slider moving a slider

		driver.get("http://demo.automationtesting.in/Slider.html");
		Actions act7 = new Actions(driver);	
		WebElement action7 = driver.findElement(By.xpath("//*[@id=\'slider\']/a"));
		Thread.sleep(5000);	

		act7.moveToElement(action7).dragAndDropBy(action7, 300, 0).build().perform();
		 */


		//Scrolling down using pixel number
		//Use javascriptexecutor create an object and use that object		

		driver.get("https://www.countries-ofthe-world.com/flags-of-the-world.html");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Thread.sleep(2000);
		js.executeScript("window.scrollBy(0,500)", ""); // it will scroll the page by pixel




		// Scroll to a certain element
		WebElement ele = driver.findElement(By.xpath("//*[@id=\'content\']/div[2]/div[2]/table[1]/tbody/tr[16]/td[1]/img"));
		Thread.sleep(2000);
		js.executeScript("arguments[0].scrollIntoView();",  ele);      // Scroll the page till element is visible

		
		
		//Scroll till the bottom of the page
		Thread.sleep(2000);
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");   // Scroll the pahge till end of the page







		Thread.sleep(5000);
		driver.quit();

	}
}
