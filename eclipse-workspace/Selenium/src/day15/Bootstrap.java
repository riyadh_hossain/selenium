package day15;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Bootstrap {

	public static void main(String[] args) throws InterruptedException {
	
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.automationtesting.in/Register.html");	
		
		//Thread.sleep(5000);;
		
		//Bootstrap web element. ABove url language is a bootstarp webelement and country is another kind of bootstrap element
		
		driver.findElement(By.xpath("//*[@id=\'msdd\']")).click();
		//driver.findElement(By.cssSelector("div#msdd")).click();// same thing as above using css locator
		
		
		
		/*
		////*[@id="basicBootstrapForm"]/div[7]/div/multi-select/div[2]/ul/li[1]/a // this is the xpath of the first item in the language drop down.
		// since we want to count all the items in the drop down we can just remove the [1] at the end of the xpath. which will give us the list of all the 
		// elements of the drop down.
		*/
		List <WebElement> langs = driver.findElements(By.xpath("//*[@id=\'basicBootstrapForm\']/div[7]/div/multi-select/div[2]/ul/li/a"));
		
		System.out.println(langs.size());
		
		langs.get(1).click();
		langs.get(0).click();
		langs.get(2).click();
		
		
		//drop down  "select skills"
		Select skills = new Select ( driver.findElement(By.xpath("//*[@id=\'Skills\']")));
		skills.selectByIndex(3);
		
		
		//another bootstarp slelct country
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("//*[@id=\'basicBootstrapForm\']/div[10]/div/span/span[1]/span")).click(); //first action
		driver.findElement(By.xpath("/html/body/span/span/span[1]/input")).sendKeys("Bangladesh");//second action to type the country 
		driver.findElement(By.xpath("/html/body/span/span/span[1]/input")).sendKeys(Keys.RETURN);// 3rd action after typing the country clicking enter
		
		
		
		
		
		
	}

}
