package day15;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Iframe {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://seleniumhq.github.io/selenium/docs/api/java/index.html");
		
		//for iframe we have to choose the frame first
		driver.switchTo().frame("packageListFrame"); //first frame
		driver.findElement(By.linkText("org.openqa.selenium")).click();
		
		//for iframe we have to choose the 2nd frame
		driver.switchTo().defaultContent(); // before we jump to 2nd frame we need to go back to main/default frame
		Thread.sleep(3000);
		driver.switchTo().frame("packageFrame"); //2nd frame
		driver.findElement(By.linkText("WebDriver")).click();
		
		//for iframe we have to choose the 3rd frame
		driver.switchTo().defaultContent(); // before we jump to 2nd frame we need to go back to main/default frame
		Thread.sleep(3000);
		driver.switchTo().frame("classFrame"); // 3rd frame
		driver.findElement(By.xpath("/html/body/div[1]/ul/li[5]/a")).click();
		
		

	}

}
