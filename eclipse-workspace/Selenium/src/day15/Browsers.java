/*
 * How to close specific tab on a browser or switch between different tabs in a browser
 * 
 */

package day15;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browsers {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Riyadh\\Desktop\\QA Automation\\Software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.automationtesting.in/Windows.html");
		
		driver.findElement(By.xpath("//*[@id=\'Tabbed\']/a/button")).click();
		
		// to get all the list of tabs open in a browser  
		Set <String> s = driver.getWindowHandles(); // this will give us the id of all the tabs opened up in a browser
		
		for(String i:s)
		{
			System.out.println(i); // printing the id of the tabs
			driver.switchTo().window(i);
			String title = driver.getTitle();
			
			if(title.equals("Sakinalium | Home"))
			{
				driver.close();
			}
		}
		
		
		

	}

}
