package day21;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Annotationsexample1 {

	@BeforeMethod
	void login()
	{
		System.out.println("Login method");
	}
	
	@AfterMethod
	void loogout()
	{
		System.out.println("Logout Method");
	}
	
	@Test(priority =1)
	void search()
	{
		System.out.println("Search Method");
		
	}
	
	
	@Test(priority =2)
	void advancesearch()
	{
		System.out.println("AdvanceSearch Method");
	}

	@Test(priority =3)
	void recharge()
	{
		System.out.println("Recharge Method");
	}
	
}

