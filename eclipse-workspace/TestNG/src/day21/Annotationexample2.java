package day21;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Annotationexample2 {
	
	@BeforeClass
	void login()
	{
		System.out.println("Login class");
	}

	
	@AfterClass
	void logout()
	{
		System.out.println("Logout class");
	}
	
	
	@Test(priority =1)
	void search()
	{
		System.out.println("Search Method");
		
	}
	
	
	@Test(priority =2)
	void advancesearch()
	{
		System.out.println("AdvanceSearch Method");
	}

	@Test(priority =3)
	void recharge()
	{
		System.out.println("Recharge Method");
	}
	
}
