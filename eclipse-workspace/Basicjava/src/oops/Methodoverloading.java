package oops;

// method overloading is having multiple method with the same name but different arguments. So calling same methods will be defined by the arguments thats getting passed.


public class Methodoverloading {


	String watchbrands;
	int price;

	public void setdata(String brand) {
watchbrands = brand;
price = 100;
	}

	public void setdata(int newprice) {
price = newprice;
watchbrands = "universal";
	}


	public void setdata(String brand, int newprice) {
watchbrands = brand;
price = newprice;
	}


	public static void main(String[] args) {

		Methodoverloading fossil = new Methodoverloading();
		fossil.setdata(70);
		
		Methodoverloading ck = new Methodoverloading();
		ck.setdata("ck");
		
		
		
		
		
	}

}
