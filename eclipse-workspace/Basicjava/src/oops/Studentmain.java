package oops;

public class Studentmain {

	public static void main(String[] args) {
		
		//constructor overloading
		student rid = new student();
		System.out.println(rid.name);
		
		
		student tis = new student("tis", 12, "phys", 86);
		System.out.println(tis.course);
		System.out.println(tis.schoolid); // static can be accessed with object 
		System.out.println(student.schoolid); // static can be used as classname.
		
	}



}
